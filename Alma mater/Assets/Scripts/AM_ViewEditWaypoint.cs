﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AM_ViewEditWaypoint : MonoBehaviour
{
    [SerializeField] private UnityEngine.UI.Button btnSubmit;
    [SerializeField] private UnityEngine.UI.Button btnCancel;
    [SerializeField] private UnityEngine.UI.Button btnDelete;
    [SerializeField] private UnityEngine.UI.Button btnAnon;

    [SerializeField] private TMPro.TextMeshProUGUI txtTitle;
    [SerializeField] private TMPro.TextMeshProUGUI txtStory;
    [SerializeField] private TMPro.TextMeshProUGUI txtCredit;

    [SerializeField] private TMPro.TMP_InputField inputTitle;
    [SerializeField] private TMPro.TMP_InputField inputStory;
    [SerializeField] private TMPro.TMP_InputField inputCredit;
    Animator a;

    AM_Controller.Waypoint currWaypoint;
    AM_Controller.Story currStory;

    private bool isCurrentWaypointNew;
    private bool isAnon = false;

    internal void Init()
    {
        a = GetComponent<Animator>();
        UnityEngine.Assertions.Assert.IsNotNull(a);
        UnityEngine.Assertions.Assert.IsNotNull(btnSubmit);
        UnityEngine.Assertions.Assert.IsNotNull(btnCancel);
        UnityEngine.Assertions.Assert.IsNotNull(btnDelete);
        UnityEngine.Assertions.Assert.IsNotNull(btnAnon);

        UnityEngine.Assertions.Assert.IsNotNull(txtTitle);
        UnityEngine.Assertions.Assert.IsNotNull(txtStory);
        UnityEngine.Assertions.Assert.IsNotNull(txtCredit);

        UnityEngine.Assertions.Assert.IsNotNull(inputTitle);
        UnityEngine.Assertions.Assert.IsNotNull(inputStory);
        UnityEngine.Assertions.Assert.IsNotNull(inputCredit);

        btnSubmit.onClick.AddListener(delegate { OnSubmit(); });
        btnCancel.onClick.AddListener(delegate { OnCancel(); });
        btnDelete.onClick.AddListener(delegate { OnDelete(); });
        btnAnon.onClick.AddListener(delegate { OnAnonSelected(); });
        inputStory.onSelect.AddListener(delegate {
            AM_Controller.instance.userHasMapInputControl = false;
            AM_Controller.instance.SuspendSpawnControl(); });
        inputStory.onDeselect.AddListener(delegate {
            AM_Controller.instance.userHasMapInputControl = true;
            AM_Controller.instance.AllowSpawnControl(); });
        inputTitle.onSelect.AddListener(delegate {
            AM_Controller.instance.userHasMapInputControl = false;
            AM_Controller.instance.SuspendSpawnControl(); });
        inputTitle.onDeselect.AddListener(delegate {
            AM_Controller.instance.userHasMapInputControl = true;
            AM_Controller.instance.AllowSpawnControl(); });
        gameObject.SetActive(false);
    }

    private void OnAnonSelected()
    {
        isAnon = !isAnon;
        inputCredit.placeholder.GetComponent<TMPro.TextMeshProUGUI>().text = isAnon
            ? ("Anonymous" + currStory.credit.Substring(currStory.credit.IndexOf(",")))
            : currStory.credit;
    }

    private void OnDelete()
    {
        AM_Controller.instance.DeleteStory(currStory);

        currStory.rating = -10;
        currStory.version += 1;
        AM_Googlesheets.PostData(this, currStory);

        gameObject.SetActive(false);
        //a.SetBool("OnScreen", false);
        AM_Controller.instance.CancelLatestWaypointIconConstuction();
    }

    internal void OpenForView(AM_Controller.Waypoint waypoint, List<AM_Controller.Story> stories)
    {
        var txtClose = btnCancel.transform.parent.GetComponentInChildren<TMPro.TextMeshProUGUI>();
        UnityEngine.Assertions.Assert.IsNotNull(txtClose);
        txtClose.text = "Close";
        btnSubmit.transform.parent.gameObject.SetActive(false);
        btnDelete.transform.parent.gameObject.SetActive(false);

        gameObject.SetActive(true);

        //a.SetBool("OnScreen", true);
        inputTitle.gameObject.SetActive(false);
        inputStory.gameObject.SetActive(false);
        inputCredit.gameObject.SetActive(false);
        txtTitle.text = waypoint.title;
        txtStory.text = stories[0].body;
        txtCredit.text = stories[0].credit;
    }

    public void OpenNewWaypointForEdit(float x, float y)
    {
        isAnon = false;

        AM_Controller.Waypoint newWaypoint = new AM_Controller.Waypoint();
        newWaypoint.x = x;
        newWaypoint.y = y;
        newWaypoint.id = AM_Controller.IdGenerator();

        currWaypoint = newWaypoint;

        AM_Controller.Story newStory = new AM_Controller.Story();
        newStory.storyID = AM_Controller.IdGenerator();
        newStory.userID = AM_Controller.instance.userID;
        newStory.wayPointID = currWaypoint.id;
        newStory.rating = 0;
        newStory.body = "";
        newStory.credit = AM_Controller.instance.userCredit;
        newStory.color = ColorUtility.ToHtmlStringRGB(AM_Controller.instance.userColor);

        currStory = newStory;

        //List<AM_Controller.Story> stories = new List<AM_Controller.Story>();
        //stories.Add(newStory);

        OpenWaypointForEdit(true);
    }

    private bool CanEditCurrStory()
    {
        return (currStory.userID.Equals(AM_Controller.instance.userID)
            || AM_Controller.instance.debug_AlwayAllowEdit);
    }

    public void OpenOldWaypointForEdit(AM_Controller.Waypoint oldWaypoint,
       List<AM_Controller.Story> stories)
    {
        // FInd the first story with a rating thats above minimum
        currStory.storyID = null;
        foreach (var s in stories)
        {
            if (s.rating > AM_Controller.k_minRating)
            {
                currStory = s;
                break;
            }
        }
        UnityEngine.Assertions.Assert.IsNotNull(currStory.storyID);
        currWaypoint = oldWaypoint;
        OpenWaypointForEdit(false);
    }

    // currStory and currWaypoint must be assigned before this call
    private void OpenWaypointForEdit(bool waypointNew)
    {
        isCurrentWaypointNew = waypointNew;

        txtTitle.text = currWaypoint.title;
        inputTitle.text = currWaypoint.title;
        inputTitle.gameObject.SetActive(waypointNew);

        txtStory.text = currStory.body;
        inputStory.text = currStory.body;
        inputStory.gameObject.SetActive(true);

        txtCredit.text = "";
        inputCredit.placeholder.GetComponent<TMPro.TextMeshProUGUI>().text
            = currStory.credit;
        inputCredit.gameObject.SetActive(true);

        var txtCancel = btnCancel.transform.parent.GetComponentInChildren<TMPro.TextMeshProUGUI>();
        UnityEngine.Assertions.Assert.IsNotNull(txtCancel);
        txtCancel.text = "Close";

        btnSubmit.transform.parent.gameObject.SetActive(true);

        bool canDelete = (CanEditCurrStory() && !waypointNew);
        btnDelete.transform.parent.gameObject.SetActive(canDelete);

        gameObject.SetActive(true);
        //a.SetBool("OnScreen", true);
    }

    internal void OnCancel()
    {
        gameObject.SetActive(false);
        //a.SetBool("OnScreen", false);
        if (AM_Controller.instance.isEditing && isCurrentWaypointNew)
            AM_Controller.instance.CancelLatestWaypointIconConstuction();
    }

    void OnSubmit()
    {
        if (inputStory.text.Length < 3)
        {
            inputStory.text += "!";
            return;
        }

        if (inputTitle.text.Length < 3)
        {
            inputTitle.text += "!";
            return;
        }


        currStory.rating = 1;
        currStory.color = isAnon
            ? ColorUtility.ToHtmlStringRGB(Color.white)
            : AM_Controller.instance.userColorString;
        currStory.userID = AM_Controller.instance.userID;
        currStory.credit = inputCredit.placeholder.GetComponent<TMPro.TextMeshProUGUI>().text;
        currStory.body = inputStory.text;
        currStory.version += 1;

        currWaypoint.version += 1;
        currWaypoint.title = inputTitle.text;

        AM_Googlesheets.PostData(this, currStory);
        AM_Googlesheets.PostData(this, currWaypoint);
        gameObject.SetActive(false);
        //a.SetBool("OnScreen", false);
        AM_Controller.instance.SaveCurrentWaypointLocally(currStory, currWaypoint);
        AM_Controller.instance.userHasMapInputControl = true;
    }

}
