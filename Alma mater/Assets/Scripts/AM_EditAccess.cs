﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AM_EditAccess : MonoBehaviour
{
    [SerializeField] private UnityEngine.UI.Button btnSubmit;
    [SerializeField] private TMPro.TextMeshProUGUI txtResult;
    [SerializeField] private TMPro.TMP_InputField inputSUNet_AtStanford;
    [SerializeField] private TMPro.TMP_InputField inputSUNet_AtXDotStanford;
    [SerializeField] private TMPro.TMP_InputField inputX_AtXDotStanford;
    private int loginCode = 0;

    public void Init()
    {
        UnityEngine.Assertions.Assert.IsNotNull(btnSubmit);
        UnityEngine.Assertions.Assert.IsNotNull(txtResult);
        UnityEngine.Assertions.Assert.IsNotNull(inputSUNet_AtStanford);
        UnityEngine.Assertions.Assert.IsNotNull(inputSUNet_AtXDotStanford);
        UnityEngine.Assertions.Assert.IsNotNull(inputX_AtXDotStanford);
        btnSubmit.onClick.AddListener(delegate { TrySubmit(); });

        loginCode = UnityEngine.Random.Range(1111, 9999);
        gameObject.SetActive(false);

        btnSubmit.transform.parent.gameObject.SetActive(true);
        txtResult.transform.parent.gameObject.SetActive(false);
    }

    private void TrySubmit()
    {
        if (inputSUNet_AtStanford.text.Length < 2
            && inputSUNet_AtXDotStanford.text.Length < 2
            && inputX_AtXDotStanford.text.Length < 2)
        {
            inputSUNet_AtStanford.text += "!";
            return;
        }

        string userEmailAddress = "";
        if (inputSUNet_AtStanford.text.Length >= 2)
        {
            userEmailAddress = inputSUNet_AtStanford.text + "@stanford.edu";
            AM_Mailmain.SendMailRequest(userEmailAddress, loginCode, OnSubmitted, this);
        }
        if (inputSUNet_AtXDotStanford.text.Length >= 2 && inputX_AtXDotStanford.text.Length >= 2)
        {
            userEmailAddress = inputSUNet_AtXDotStanford.text + "@" + inputX_AtXDotStanford.text + ".stanford.edu";
            AM_Mailmain.SendMailRequest(userEmailAddress, loginCode, OnSubmitted, this);
        }
    }

    private void OnSubmitted(string response)
    {
        btnSubmit.transform.parent.gameObject.SetActive(false);
        txtResult.transform.parent.gameObject.SetActive(true);
        txtResult.text = response;
        Invoke("SetShowingFalse", 1);
        Invoke("OpenEnterCodePrompt", 2f);
    }

    internal void OpenEnterCodePrompt()
    {
        AM_Controller.instance.OpenEnterCodePrompt( "" + loginCode);
    }

    internal void SetShowingFalse()
    {
        gameObject.SetActive(false);
    }

    internal void SetShowing(bool v)
    {
        gameObject.SetActive(v);
    }
}
