﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class AM_Controller : MonoBehaviour
{

    [SerializeField] private AE_EnterCode controller_editCode;
    [SerializeField] private AM_EditAccess controller_editAccess;
    [SerializeField] private AM_EditLogin controller_editLogin;
    [SerializeField] private AM_Slider controller_slider;
    [SerializeField] private AM_CameraScroller controller_cameraScroller;
    [SerializeField] private AM_ViewEditWaypoint controller_viewEdit;
    [SerializeField] private AM_WaypointController prefab_waypoint;
    [SerializeField] private AM_Menu controller_menu;
    //[SerializeField] private GameObject colorPicker;
    [SerializeField] private GameObject canvasUI;
    [SerializeField] private GameObject canvasColorPicker;
    [SerializeField] private UnityEngine.UI.Image imgColorDisplay;

    [SerializeField] private bool debug_SkipIntro;
    [SerializeField] private bool debug_ClearDataOnStart;
    [SerializeField] public bool debug_AlwaysRequestEditAccess;

    [SerializeField] public bool debug_AlwayAllowEdit;
    private float timeWhenWeAreAllowedToContinueSpawning;

    // Forces compiler to include these files: https://issuetracker.unity3d.com/issues/il2cpp-crashes-when-using-smtpclient
    public void Includes()
    {
        //var incl0 = new System.Net.Configuration.MailSettingsSectionGroup();
        //var incl1 = new System.Net.Configuration.SmtpSection();
        //var incl2 = new System.Net.Configuration.SmtpNetworkElement();
    }

    internal void OnWaypointSelected(Waypoint waypoint, List<Story> stories, AM_WaypointController waypointController)
    {
        latestWaypointIcon = waypointController;
        if (isEditing)
        {
            bool allowEdit = false;
            foreach (var story in stories)
                if (story.userID.Equals(userID))
                    allowEdit = true;
            if (allowEdit || debug_AlwayAllowEdit)
                controller_viewEdit.OpenOldWaypointForEdit(waypoint, stories);
        }
        else
            controller_viewEdit.OpenForView(waypoint, stories);
    }

    internal void ClosePopups()
    {
        controller_editLogin.gameObject.SetActive(false);
        controller_viewEdit.OnCancel();
        controller_editAccess.SetShowingFalse();
        controller_editCode.gameObject.SetActive(false);
    }

    [SerializeField] private UnityEngine.UI.Button btnSubmitColorDisplay;
    [SerializeField] private Transform parent_waypoints;
    //[SerializeField] private UnityEngine.UI.Button btnMap;
    public UnityEngine.UI.Image imgTouchTrap;
    public bool userHasMapInputControl { get; set; }
    //public bool editAccessGranted { get; set; }
    public bool isEditing = false;
    private AM_WaypointController latestWaypointIcon;
    public const int k_minRating = -10;

    internal void OpenEnterCodePrompt(string code)
    {
        controller_editCode.Open(code);
    }

    internal void ClickSlider()
    {
        controller_slider.OnSliderClicked();
    }

    public string userID
    {
        get
        {
            string id = PlayerPrefs.GetString("PlayerID", null);
            if (string.IsNullOrEmpty(id))
            {
                Debug.Log("generating id");
                id = IdGenerator();
                PlayerPrefs.SetString("PlayerID", id);
            }
            return id;
        }
        private set
        {
            Debug.LogError("Can't set this value.");
        }
    }

    internal void DeleteStory(Story story)
    {
        latestWaypointIcon.DeleteStory(story);
    }

    public string userCredit
    {
        get
        {
            return PlayerPrefs.GetString("userCredit");
        }
        private set
        {
            PlayerPrefs.SetString("userCredit", value);
        }
    }

    public Color stringToColor(string userColor)
    {

        // Format and fill alpha
        userColor = "#" + userColor + "AA";

        Color x;
        bool success = ColorUtility.TryParseHtmlString(userColor, out x);
        if (success)
        {
            return x;
        }
        else
        {
            Debug.LogError("Couldn't parse user color.");
            return AM_WaypointController.k_defaultColor;
        }
    }

    public Color userColor
    {
        get
        {
            String userColor = PlayerPrefs.GetString("userColor", null);
            if (userColor == null)
            {
                Debug.LogError("Couldn't find user color.");
                return AM_WaypointController.k_defaultColor;
            }
            return stringToColor(userColor);
        }
        private set
        {
            PlayerPrefs.SetString("userColor", ColorUtility.ToHtmlStringRGB(value));
        }
    }
    public String userColorString
    {
        get
        {
            return PlayerPrefs.GetString("userColor", null);
        }
        private set
        {
            Debug.LogError("You shoudl be setting the Color userColor not the String.");
        }
    }

    internal void SetColorPickerVisible(bool v)
    {
        controller_cameraScroller.gameObject.SetActive(!v);
        canvasColorPicker.SetActive(v);
        canvasUI.SetActive(!v);
    }

    public static string IdGenerator()
    {
        int k_idLength = 10;
        StringBuilder builder = new StringBuilder();
        char ch;
        for (int i = 0; i < k_idLength; i++)
        {
            ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * UnityEngine.Random.Range(0.0f, 1.0f) + 65)));
            builder.Append(ch);
        }
        return builder.ToString();
    }

    public static AM_Controller _instance;
    public static AM_Controller instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("Controller instance died, trying to find it...");
                var controller = GameObject.FindObjectOfType<AM_Controller>();
                _instance = controller.GetComponent<AM_Controller>();
                return _instance;
            }
            else
            {
                return _instance;
            }
        }
        set { _instance = value; }
    }

    internal void SaveCurrentWaypointLocally(Story newStory, Waypoint newWaypoint)
    {
        if (null == latestWaypointIcon)
            return;
        List<Story> newStoryList = new List<Story>();
        newStoryList.Add(newStory);
        latestWaypointIcon.Setup(newStoryList, newWaypoint);
        latestWaypointIcon = null;
    }

    internal void SubmitUserInfo(string newCredit, Color newColor)
    {
        userColor = newColor;
        userCredit = newCredit;
        GiveUserControlAgain();
        controller_slider.OnSliderClicked();
    }

    public void DownloadData()
    {
        StartCoroutine(AM_Googlesheets.DownloadData(DownloadDataComplete));
    }

    private void DownloadDataComplete(GameData data)
    {
        // If duplicate waypoint id's, remove the ones with old version
        Dictionary<string, Waypoint> upToDateWaypoints = new Dictionary<string, Waypoint>();
        foreach (Waypoint currWaypoint in data.waypoints)
        {
            if (upToDateWaypoints.ContainsKey(currWaypoint.id))
            {
                if (currWaypoint.version > upToDateWaypoints[currWaypoint.id].version)
                    upToDateWaypoints[currWaypoint.id] = currWaypoint;
            }
            else
            {
                upToDateWaypoints[currWaypoint.id] = currWaypoint;
            }
        }

        // Find up to date stories for each waypoint and construct the waypoint
        foreach (KeyValuePair<string, Waypoint> entry in upToDateWaypoints)
        {
            if (!data.waypointStories.ContainsKey(entry.Key))
            {
                Debug.LogWarning("No stories were listed for the onserved waypoint id: " + entry.Key);
                continue;
            }

            // If duplicate story id's, remove the ones with old version
            Dictionary<string, Story> upToDateStories = new Dictionary<string, Story>();
            foreach (Story currStory in data.waypointStories[entry.Key])
            {
                if (upToDateStories.ContainsKey(currStory.storyID))
                {
                    if (currStory.version > upToDateStories[currStory.storyID].version)
                        upToDateStories[currStory.storyID] = currStory;
                }
                else
                {
                    upToDateStories[currStory.storyID] = currStory;
                }
            }

            // Construct a new waypoint with these up to date stories
            AM_WaypointController newWaypointIcon = Instantiate(prefab_waypoint, parent_waypoints);
            newWaypointIcon.transform.position = new Vector3(entry.Value.x, entry.Value.y, 0);
            newWaypointIcon.Setup(upToDateStories.Values.ToList(), entry.Value);
        }
    }

    public void OnColorSelected(Color newColor)
    {
        imgColorDisplay.color = newColor;
        userColor = newColor;
    }

    void Init()
    {
        if (debug_AlwaysRequestEditAccess)
            PlayerPrefs.SetInt("editAccess", 0);
        if (debug_ClearDataOnStart)
            PlayerPrefs.DeleteAll();
        UnityEngine.Assertions.Assert.IsNotNull(controller_editCode);
        UnityEngine.Assertions.Assert.IsNotNull(controller_viewEdit);
        UnityEngine.Assertions.Assert.IsNotNull(controller_editAccess);
        UnityEngine.Assertions.Assert.IsNotNull(controller_cameraScroller);
        UnityEngine.Assertions.Assert.IsNotNull(controller_slider);
        UnityEngine.Assertions.Assert.IsNotNull(imgTouchTrap);
        UnityEngine.Assertions.Assert.IsNotNull(prefab_waypoint);
        UnityEngine.Assertions.Assert.IsNotNull(parent_waypoints);
        UnityEngine.Assertions.Assert.IsNotNull(canvasUI);
        UnityEngine.Assertions.Assert.IsNotNull(canvasColorPicker);
        UnityEngine.Assertions.Assert.IsNotNull(controller_menu);
        //UnityEngine.Assertions.Assert.IsNotNull(btnMap);
        UnityEngine.Assertions.Assert.IsNotNull(btnSubmitColorDisplay);
        btnSubmitColorDisplay.onClick.AddListener(delegate { SetColorPickerVisible(false); });
        //btnMap.onClick.AddListener(delegate { OnMapClicked(); });
    }

    internal void RequestLogin()
    {
        userHasMapInputControl = false;
        controller_editLogin.Open();
    }

    internal void CancelLatestWaypointIconConstuction()
    {
        if (null != latestWaypointIcon)
        {
            Destroy(latestWaypointIcon.gameObject);
        }
        userHasMapInputControl = true;
    }

    public void SuspendUserControl(float numSeconds)
    {
        CancelInvoke("GiveUserControlAgain");
        userHasMapInputControl = false;
        Invoke("GiveUserControlAgain", numSeconds);
    }

    private void GiveUserControlAgain()
    {
        userHasMapInputControl = true;
    }

    void Start()
    {
        Includes();
        Init();
        InitSubControllers();
        DownloadData();
        instance = this;
        userHasMapInputControl = false;
    }

    private void InitSubControllers()
    {
        controller_editAccess.Init();
        controller_editLogin.Init();
        controller_slider.Init();
        controller_cameraScroller.Init();
        controller_viewEdit.Init();
        controller_menu.Init();
        controller_editCode.gameObject.SetActive(false);
    }

    public void StartIntroduction()
    {
        if (debug_SkipIntro)
        {
            FinishIntroduction();
            return;
        }

        // TODO fade
        controller_viewEdit.OnCancel();
        imgTouchTrap.gameObject.SetActive(true);
        controller_cameraScroller.BeginIntroductionZoomOut();
    }

    internal void FinishIntroduction()
    {
        imgTouchTrap.gameObject.SetActive(false);
        userHasMapInputControl = true;
    }

    public struct GameData
    {
        public Dictionary<string, List<Story>> waypointStories;
        public List<Waypoint> waypoints;
    }

    [Serializable]
    public struct Story
    {
        public int rating, version;
        public string storyID, userID, wayPointID, body, credit, color;

        internal bool IsValid()
        {
            bool isValid = true;
            isValid &= !string.IsNullOrEmpty(storyID);
            // TODO: isValid &= !string.IsNullOrEmpty(userID);
            isValid &= !string.IsNullOrEmpty(wayPointID);
            isValid &= !string.IsNullOrEmpty(body);
            isValid &= !string.IsNullOrEmpty(credit);
            isValid &= !string.IsNullOrEmpty(color);
            return isValid;
        }
    }

    [Serializable]
    public struct Waypoint
    {
        public int version;
        public String title, id;
        public float x, y;

        internal bool IsValid()
        {
            bool isValid = true;
            isValid &= !string.IsNullOrEmpty(title);
            isValid &= !string.IsNullOrEmpty(id);
            return isValid;
        }
    }

    public void SuspendSpawnControl()
    {
        //Debug.Log("Map controls suspended");
        timeWhenWeAreAllowedToContinueSpawning = float.MaxValue;
    }

    public void AllowSpawnControl()
    {
        //Debug.Log("Map controls allowed soon");
        float delay = 0.5f;
        timeWhenWeAreAllowedToContinueSpawning = Time.timeSinceLevelLoad + delay;
    }

    internal void OnMapClicked()
    {
        if (!isEditing)
            return;
        if (!userHasMapInputControl)
            return;
        if (Time.timeSinceLevelLoad <= timeWhenWeAreAllowedToContinueSpawning)
            return;

        // Create waypoint
        AM_WaypointController newWaypointIcon = Instantiate(prefab_waypoint, parent_waypoints);
        var screenPoint = Input.mousePosition;
        float cameraToCanvas = parent_waypoints.transform.position.z - Camera.main.transform.position.z;
        screenPoint.z = cameraToCanvas;
        newWaypointIcon.gameObject.SetActive(true);

        var img = newWaypointIcon.GetComponent<UnityEngine.UI.Image>();
        UnityEngine.Assertions.Assert.IsNotNull(img);
        img.color = userColor;

        newWaypointIcon.transform.position = Camera.main.ScreenToWorldPoint(screenPoint);
        latestWaypointIcon = newWaypointIcon;

        // Position camera appropriately
        // TODO

        // Open edit menu
        controller_viewEdit.OpenNewWaypointForEdit(newWaypointIcon.transform.position.x, newWaypointIcon.transform.position.y);
        userHasMapInputControl = false;

    }

    internal void RequestEditAccess()
    {
        userHasMapInputControl = false;
        controller_editAccess.SetShowing(true);
    }
}
