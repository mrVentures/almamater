﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using Google.Apis.Auth.OAuth2;
//using Google.Apis.Sheets.v4;
//using Google.Apis.Sheets.v4.Data;
//using Google.Apis.Services;
//using Google.Apis.Util.Store;
//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Threading;

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Linq;

public static class AM_Googlesheets {
    private const string k_googleSheetDocID = "1zPfT6-DIoYJZ0-YF4BPpLjngw_obvpHUAAyp4j0sNG4";
    private const string k_googleFormURL = "https://docs.google.com/forms/d/e/1FAIpQLSdFXQvyXMd7QK_ss0sB_WeFosiQzF80IxYuZNOIegVEhuBN_A/formResponse";
    private const string k_googleFormEntryID = "entry.1428624036";

    internal static IEnumerator DownloadData(Action<AM_Controller.GameData> onCompleted) {
        string url = "https://docs.google.com/spreadsheets/d/" + k_googleSheetDocID + "/export?format=csv";
        WWWForm form = new WWWForm();
        WWW downloadReceipt = new WWW(url, form);

        yield return downloadReceipt;

        string downloadData;
        if(!string.IsNullOrEmpty(downloadReceipt.error)) {
            Debug.Log("Download Error: " + downloadReceipt.error);
            downloadData = PlayerPrefs.GetString("LastDataDownloaded", null);
        }
        else {
            //Debug.Log("Download success: " + downloadReceipt.text);
            PlayerPrefs.SetString("LastDataDownloaded", downloadReceipt.text);
            downloadData = downloadReceipt.text;
        }

        if(string.IsNullOrEmpty(downloadData)) {
            AM_Controller.GameData blankData = new AM_Controller.GameData();
            blankData.waypoints = new List<AM_Controller.Waypoint>();
            blankData.waypointStories = new Dictionary<string, List<AM_Controller.Story>>();
            onCompleted(blankData);
        }
        else {
            //Debug.Log("Downloaded: " + downloadData);
            var data = ExtractDataFromDownload(downloadData);
            onCompleted(data);
        }
    }

    private static AM_Controller.GameData ExtractDataFromDownload(string serializedData) {
        AM_Controller.GameData deserializedData = new AM_Controller.GameData();
        deserializedData.waypoints = new List<AM_Controller.Waypoint>();
        deserializedData.waypointStories = new Dictionary<string, List<AM_Controller.Story>>();

        foreach(string dataEntry in serializedData.Split('\n')) {
            int startIndex = dataEntry.IndexOf("{");
            int endIndex = dataEntry.LastIndexOf("}");

            // Skip invalid or blank entries
            if(startIndex == -1 || endIndex == -1)
                continue;

            // Trim to just inside of brackets
            string trimmedEntry = dataEntry.Substring(startIndex, (endIndex + 1) - startIndex);

            // Replace double quotes with just quotes
            string backslash = "\"";
            trimmedEntry = trimmedEntry.Replace(backslash + backslash, backslash);

            // Try parse story
            AM_Controller.Story story = JsonUtility.FromJson<AM_Controller.Story>(trimmedEntry);
            
            if (story.IsValid()) {
                bool waypointIsNew = !deserializedData.waypointStories.ContainsKey(story.wayPointID);

                List<AM_Controller.Story> stories;
                if(waypointIsNew)
                    stories = new List<AM_Controller.Story>();
                else
                    stories = deserializedData.waypointStories[story.wayPointID];

                stories.Add(story);
                deserializedData.waypointStories[story.wayPointID] = stories;
                //Debug.Log("######### Downloaded story with wID: " + story.wayPointID);
                continue;
            }

            // Try parse waypoint
            AM_Controller.Waypoint waypoint = JsonUtility.FromJson<AM_Controller.Waypoint>(trimmedEntry);
            if(waypoint.IsValid()) {
                deserializedData.waypoints.Add(waypoint);
                //Debug.Log("-- Downloaded waypoint with wID: " + waypoint.id);
                continue;
            }

            
            // It was neither a waypoint nor story
            //Debug.Log("All parse attempts failed for input: " + trimmedEntry);
        }

        Debug.LogWarning("Download COmplete, time to load");
        return deserializedData;
    }

    internal static void PostData<T>(MonoBehaviour caller, T dataContainer) {
        caller.StartCoroutine(PostData<T>(dataContainer));
    }

    private static IEnumerator PostData<T>(T dataContainer) {
        string jsonData = JsonUtility.ToJson(dataContainer);

        WWWForm form = new WWWForm();
        form.AddField(k_googleFormEntryID, jsonData);
        WWW www = new WWW(k_googleFormURL, form.data);

        Debug.Log("Posting " + dataContainer + ": " + jsonData);
        yield return www;
    }
}
