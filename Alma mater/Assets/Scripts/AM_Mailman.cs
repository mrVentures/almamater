﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

public static class AM_Mailmain
{

    static IEnumerator SendMailRequestToServer(string address, int code, Action<string> onSubmitted)
    {
        Debug.Log("Starting email request...");

        string url = "https://almamatergame.000webhostapp.com/index.php";
        string message = "Enter this code into Alma mater: " + code;

        WWWForm form = new WWWForm();
        form.AddField("name", "Alma mater");
        form.AddField("fromEmail", "almamatermailman@gmail.com");
        form.AddField("toEmail", address);
        form.AddField("message", message);
        WWW www = new WWW(url, form);

        yield return www;

        Debug.Log("...Emailing done.");

        if (www.error == null)
        {
            Debug.Log("WWW Ok!: " + www.text);
            onSubmitted(www.text);
        }
        else
        {
            Debug.Log("WWW Error: " + www.error);
            onSubmitted(www.error);
        }
    }

    internal static void SendMailRequest(string address, int code, Action<string> onSubmitted, MonoBehaviour m)
    {
        m.StartCoroutine(SendMailRequestToServer(address, code, onSubmitted));
    }

    internal static void SendMail_DEPRECATED(string address, int code, Action<string> onSubmitted)
    {

        // This method to manually send the email does not work in WebGL
        // we instead needed to host a php script which could do the mailing for us

        Debug.Log("Sending email to:" + address);

        // Create mail
        MailMessage mail = new MailMessage();
        mail.From = new MailAddress("almamatermailman@gmail.com");
        mail.To.Add(address);
        mail.Subject = "Your Alma mater login";
        mail.Body = "Please enter the following code: " + code;

        // Setup server 
        SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
        smtpServer.Port = 587;
        smtpServer.Credentials = new System.Net.NetworkCredential(
            "almamatermailman@gmail.com", "Die Luft der Freiheit weht")
            as ICredentialsByHost;
        smtpServer.EnableSsl = true;
        ServicePointManager.ServerCertificateValidationCallback =
            delegate (object s, X509Certificate certificate,
            X509Chain chain, SslPolicyErrors sslPolicyErrors)
            {
                Debug.Log("Delegate");
                return true;
            };

        // Send mail to server
        string errorMessage = null;
        try
        {
            smtpServer.Send(mail);
        }
        catch (Exception e)
        {
            errorMessage = e.GetBaseException().Message;
            Debug.Log(e.HelpLink);
            Debug.Log(e.InnerException.Message);
            Debug.Log(e.StackTrace);
            Debug.Log(e.Message);
        }
        finally
        {
            Debug.Log("Finally");
            onSubmitted(errorMessage);
            Debug.Log("Error: " + errorMessage);
        }

        Debug.Log("Email sent!");
    }
}
