﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UnityEngine.UI.Image))]
public class AM_WaypointController : MonoBehaviour {
    AM_Controller.Waypoint m_waypoint;
    List<AM_Controller.Story> m_stories;
    public static readonly Color k_defaultColor = Color.red;
    public void Setup(List<AM_Controller.Story> stories, AM_Controller.Waypoint waypoint) {
        m_stories = stories;
        m_waypoint = waypoint;

        var btn = GetComponent<AM_Button>();
        UnityEngine.Assertions.Assert.IsNotNull(btn);
        btn.onClick.RemoveAllListeners();
        btn.onClick.AddListener(delegate { OnClicked(); });

        var img = GetComponent<UnityEngine.UI.Image>();
        UnityEngine.Assertions.Assert.IsNotNull(img);
        Color c = AM_Controller.instance.stringToColor(m_stories[0].color);
        img.color = c;

        gameObject.SetActive(true);
        DeleteSelfIfPoorRatings();
    }

    private void OnClicked() {
        if(!AM_Controller.instance.userHasMapInputControl)
            return;
        AM_Controller.instance.OnWaypointSelected(m_waypoint, m_stories, this);
    }

    void DeleteSelfIfPoorRatings() {
        int numStoriesWithSufficientRating = 0;

        for(int i = 0; i < m_stories.Count; i++) {
            if(m_stories[i].rating > AM_Controller.k_minRating)
                numStoriesWithSufficientRating++;
        }

        if(numStoriesWithSufficientRating <= 0)
            Destroy(this.gameObject);
    }

    internal void DeleteStory(AM_Controller.Story storyToDelete) {
        storyToDelete.rating = AM_Controller.k_minRating;
        for(int i = 0; i < m_stories.Count; i++) {
            if(m_stories[i].storyID.Equals(storyToDelete.storyID)) {
                m_stories[i] = storyToDelete;
                break;
            }
        }
        DeleteSelfIfPoorRatings();
    }
}
