﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AM_Menu : MonoBehaviour {
    [SerializeField] AM_Button btnMenu;
    [SerializeField] AM_Button btnMap;
    [SerializeField] UnityEngine.UI.Button btnInfo;
    [SerializeField] GameObject centerGraphics;
    [SerializeField] GameObject authorsNote;
    [SerializeField] UnityEngine.UI.Image imgHand;

    public void Init() {
        UnityEngine.Assertions.Assert.IsNotNull(btnMenu);
        UnityEngine.Assertions.Assert.IsNotNull(btnMap);
        UnityEngine.Assertions.Assert.IsNotNull(btnInfo);
        UnityEngine.Assertions.Assert.IsNotNull(centerGraphics);
        UnityEngine.Assertions.Assert.IsNotNull(authorsNote);
        UnityEngine.Assertions.Assert.IsNotNull(imgHand);

        btnMenu.onClick.AddListener(delegate { OnBtnMenuClicked(); });
        btnMap.onClick.AddListener(delegate { OnBtnMapClicked(); });
        btnInfo.onClick.AddListener(delegate { OnBtnInfoClicked(); });

        btnMenu.gameObject.SetActive(false);
        btnMap.gameObject.SetActive(true);
        gameObject.SetActive(true);
    }

    private void OnBtnMenuClicked() {
        gameObject.SetActive(true);
        btnMenu.gameObject.SetActive(false);
        AM_Controller.instance.ClosePopups();
    }

    private void OnBtnMapClicked() {
        gameObject.SetActive(false);
        AM_Controller.instance.StartIntroduction();
        centerGraphics.SetActive(true);
        authorsNote.SetActive(false);
        btnMenu.gameObject.SetActive(true);
        imgHand.gameObject.SetActive(false);
    }

    private void OnBtnInfoClicked() {
        centerGraphics.SetActive(!centerGraphics.activeInHierarchy);
        authorsNote.SetActive(!centerGraphics.activeInHierarchy);
    }

}
