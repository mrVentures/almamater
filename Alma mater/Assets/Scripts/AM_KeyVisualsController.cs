﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AM_KeyVisualsController : MonoBehaviour {

    [SerializeField] private Transform plus;
    [SerializeField] private Transform minus;
    [SerializeField] private Transform up;
    [SerializeField] private Transform down;
    [SerializeField] private Transform right;
    [SerializeField] private Transform left;

    // TODO this is redundant considering KeyCode
    public enum key { plus, minus, up, down, right, left }

    private void Start() {
        UnityEngine.Assertions.Assert.IsNotNull(plus);
        UnityEngine.Assertions.Assert.IsNotNull(minus);
        UnityEngine.Assertions.Assert.IsNotNull(up);
        UnityEngine.Assertions.Assert.IsNotNull(down);
        UnityEngine.Assertions.Assert.IsNotNull(right);
        UnityEngine.Assertions.Assert.IsNotNull(left);
    }

    private Transform GetCorrectTransform(key k) {
        switch(k) {
            case key.plus:
                return plus;
            case key.minus:
                return minus;
            case key.up:
                return up;
            case key.down:
                return down;
            case key.right:
                return right;
            case key.left:
                return left;
        }
        Debug.LogError("Failed to find key");
        return null;
    }

    public void SetInverted(key k, bool isInverted) {
        Transform keyParent = GetCorrectTransform(k);
        keyParent.GetChild(1).gameObject.SetActive(!isInverted);
        keyParent.GetChild(0).gameObject.SetActive(isInverted);
    }

    private void Update() {
        if(Input.GetKeyUp(KeyCode.Plus) || Input.GetKeyUp(KeyCode.Equals))
            SetInverted(key.plus, true);
        if(Input.GetKeyDown(KeyCode.Plus) || Input.GetKeyDown(KeyCode.Equals))
            SetInverted(key.plus, false);

        if(Input.GetKeyUp(KeyCode.Minus) || Input.GetKeyUp(KeyCode.Underscore))
            SetInverted(key.minus, true);
        if(Input.GetKeyDown(KeyCode.Minus) || Input.GetKeyDown(KeyCode.Underscore))
            SetInverted(key.minus, false);

        if(Input.GetKeyUp(KeyCode.UpArrow))
            SetInverted(key.up, true);
        if(Input.GetKeyDown(KeyCode.UpArrow))
            SetInverted(key.up, false);

        if(Input.GetKeyUp(KeyCode.DownArrow))
            SetInverted(key.down, true);
        if(Input.GetKeyDown(KeyCode.DownArrow))
            SetInverted(key.down, false);

        if(Input.GetKeyUp(KeyCode.RightArrow))
            SetInverted(key.right, true);
        if(Input.GetKeyDown(KeyCode.RightArrow))
            SetInverted(key.right, false);

        if(Input.GetKeyUp(KeyCode.LeftArrow))
            SetInverted(key.left, true);
        if(Input.GetKeyDown(KeyCode.LeftArrow))
            SetInverted(key.left, false);

    }
}
