﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;

// Button that raises onDown event when OnPointerDown is called.
[AddComponentMenu("AM_Button")]
public class AM_Button : Button
{
    [SerializeField]
    ButtonDownEvent _onDown = new ButtonDownEvent();

    [SerializeField]
    ButtonUpEvent _onUp = new ButtonUpEvent();

    protected AM_Button() { }

    public override void OnPointerDown(PointerEventData eventData)
    {
        AM_Controller.instance.SuspendSpawnControl();

        base.OnPointerDown(eventData);

        if (eventData.button != PointerEventData.InputButton.Left)
            return;

        _onDown.Invoke();
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        AM_Controller.instance.AllowSpawnControl();

        base.OnPointerUp(eventData);

        if (eventData.button != PointerEventData.InputButton.Left)
            return;

        _onUp.Invoke();
    }

    public ButtonDownEvent onDown
    {
        get { return _onDown; }
        set { _onDown = value; }
    }

    public ButtonUpEvent onUp
    {
        get { return _onUp; }
        set { _onUp = value; }
    }

    [Serializable]
    public class ButtonDownEvent : UnityEvent { }

    [Serializable]
    public class ButtonUpEvent : UnityEvent { }
}