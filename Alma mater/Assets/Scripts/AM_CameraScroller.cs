﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AM_CameraScroller : MonoBehaviour {
    public float speed = 1;
    private Vector3 inputAI = Vector3.zero;
    public bool preventMovement = false;
    //[SerializeField] UnityEngine.UI.Button btnMap;

    internal void Init() {
        //UnityEngine.Assertions.Assert.IsNotNull(btnMap);
    }

    public void BeginIntroductionZoomOut() {
        transform.position = new Vector3(-470, 1010, -300);
        inputAI = new Vector3(0, 0, -1.5f);
        AM_Controller.instance.userHasMapInputControl = false;
        Invoke("FinishIntroductionZoomOut", 5.0f);
    }

    private void FinishIntroductionZoomOut() {
        inputAI = Vector3.zero;
        AM_Controller.instance.FinishIntroduction();
    }

    float dragSpeed = 10;
    float scrollSpeed = 200;
    private Vector3 dragOrigin;

    void MouseDragMoveCamera() {
        if(!AM_Controller.instance.userHasMapInputControl)
            return;

        if(Input.GetMouseButtonDown(0)) {
            dragOrigin = Input.mousePosition;
        }

        Vector3 delta = Camera.main.ScreenToViewportPoint(dragOrigin - Input.mousePosition);

        // Spawn new waypoint
        if(Input.GetMouseButtonUp(0)) {
            if(delta.magnitude < 0.03) {
                AM_Controller.instance.OnMapClicked();
                return;
            }
        }

        if(!Input.GetMouseButton(0))
            delta = Vector3.zero;
        delta.z = Input.GetAxis("Mouse ScrollWheel");

        Vector3 move = new Vector3(
            delta.x * dragSpeed,
            delta.y * dragSpeed,
            delta.z * scrollSpeed);
        transform.position = ConstrainPosition(transform.position + move);
    }

    void Update() {
        if (!preventMovement)
        {
            MouseDragMoveCamera();
            ButtonMoveCamera();
        }
        else
        {
            Debug.Log("Open");
        }
    }


    void ButtonMoveCamera() {
        Vector3 input = Vector3.zero;
        if(AM_Controller.instance.userHasMapInputControl) {
            input.x = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
            input.y = Input.GetAxis("Vertical") * Time.deltaTime * speed;
            if(Input.GetKey(KeyCode.Plus) || Input.GetKey(KeyCode.Equals))
                input.z = 1 * Time.deltaTime * speed;
            if(Input.GetKey(KeyCode.Minus) || Input.GetKey(KeyCode.Underscore))
                input.z = -1 * Time.deltaTime * speed;
        }
        else {
            input = inputAI;
        }

        if(input == Vector3.zero)
            return;

        Vector3 desiredPosition = transform.position + input;
        transform.position = ConstrainPosition(desiredPosition);
    }

    Vector3 ConstrainPosition(Vector3 desiredPosition) {
        float xMin = -1200.0f;
        float xMax = 800.0f;
        float yMin = 22.0f;
        float yMax = 1800.0f;
        float zMin = -1700.0f;
        float zMax = -300.0f;
        desiredPosition.x = Mathf.Clamp(desiredPosition.x, xMin, xMax);
        desiredPosition.y = Mathf.Clamp(desiredPosition.y, yMin, yMax);
        desiredPosition.z = Mathf.Clamp(desiredPosition.z, zMin, zMax);
        return desiredPosition;
    }


}
