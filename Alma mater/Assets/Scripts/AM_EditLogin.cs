﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AM_EditLogin : MonoBehaviour {
    [SerializeField] private UnityEngine.UI.Button btnSave;
    [SerializeField] private TMPro.TMP_InputField inputFirstName;
    [SerializeField] private TMPro.TMP_InputField inputClassYear;
    [SerializeField] private UnityEngine.UI.Button btnColor;

    public void OnColorClicked() {
        AM_Controller.instance.SetColorPickerVisible(true);
    }

    public void Open() {
        gameObject.SetActive(true);
    }

    void OnSave() {
        // Ensure input entered
        if(inputClassYear.text.Length < 2) {
            inputClassYear.text += "!";
            return;
        }
        if(inputFirstName.text.Length < 2) {
            inputFirstName.text += "!";
            return;
        }
        string credit = inputFirstName.text + ", Class of " + inputClassYear.text;
        gameObject.SetActive(false);
        AM_Controller.instance.SetColorPickerVisible(false);
        AM_Controller.instance.SubmitUserInfo(credit, btnColor.image.color);
    }

    internal void Init() {
        UnityEngine.Assertions.Assert.IsNotNull(inputFirstName);
        UnityEngine.Assertions.Assert.IsNotNull(inputClassYear);
        UnityEngine.Assertions.Assert.IsNotNull(btnColor);
        UnityEngine.Assertions.Assert.IsNotNull(btnSave);
        btnSave.onClick.AddListener(delegate { OnSave(); });
        btnColor.onClick.AddListener(delegate { OnColorClicked(); });
        gameObject.SetActive(false);
    }
}
