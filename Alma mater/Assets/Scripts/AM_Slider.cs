﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class AM_Slider : MonoBehaviour, IPointerDownHandler
{
    private const string editing = "editing";
    private const string viewing = "viewing";
    [SerializeField] private Animator a;
    [SerializeField] private TMPro.TextMeshProUGUI txtViewEdit;
    [SerializeField] private AM_Button btnSlider;
    //public bool isEditing { get{ return a.GetBool(editing);}  }

    public void Init()
    {
        UnityEngine.Assertions.Assert.IsNotNull(a);
        UnityEngine.Assertions.Assert.IsNotNull(txtViewEdit);
        UnityEngine.Assertions.Assert.IsNotNull(btnSlider);
        a.SetBool(viewing, true);
        a.SetBool(editing, false);
        btnSlider.onClick.AddListener(delegate { OnSliderClicked(); });
        //btnSlider.onDown.AddListener(   delegate { AM_Controller.instance.preventNewWaypointCreation = true; });
        //btnSlider.onUp.AddListener(     delegate { AM_Controller.instance.preventNewWaypointCreation = false;});
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log(this.gameObject.name + " Was Clicked.");
    }

    // Called from animation
    public void AnimEvent_SwitchDirection()
    {
        if (a.GetBool("viewing"))
        {
            txtViewEdit.text = "edit";
            txtViewEdit.alignment = TMPro.TextAlignmentOptions.MidlineLeft;
        }
        else
        {
            txtViewEdit.text = "view";
            txtViewEdit.alignment = TMPro.TextAlignmentOptions.MidlineRight;
        }
    }

    public void OnSliderClicked()
    {
        if (!AM_Controller.instance.userHasMapInputControl)
            return;

        if (PlayerPrefs.GetInt("editAccess", 0) != 1)
        {
            AM_Controller.instance.RequestEditAccess();
            return;
        }

        if (null == AM_Controller.instance.userCredit
            || AM_Controller.instance.userCredit == "")
        {
            AM_Controller.instance.RequestLogin();
            return;
        }

        AM_Controller.instance.SuspendUserControl(.9f);

        // Change sliders
        if (!AM_Controller.instance.isEditing)
        {
            a.SetBool(viewing, false);
            a.SetBool(editing, true);
        }
        else if (AM_Controller.instance.isEditing)
        {
            a.SetBool(editing, false);
            a.SetBool(viewing, true);
        }
        else
        {
            Debug.LogError("Slider was not in either state.");
        }

        AM_Controller.instance.isEditing = !AM_Controller.instance.isEditing;

    }
}
