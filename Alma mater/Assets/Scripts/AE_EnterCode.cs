﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AE_EnterCode : MonoBehaviour {

    [SerializeField] TMPro.TMP_InputField inputCode;
    [SerializeField] UnityEngine.UI.Button btnSend;
    [SerializeField] TMPro.TextMeshProUGUI txtResult;
    string code = "";

    private void Start()
    {
        UnityEngine.Assertions.Assert.IsNotNull(inputCode);
        UnityEngine.Assertions.Assert.IsNotNull(btnSend);
        UnityEngine.Assertions.Assert.IsNotNull(txtResult);
        btnSend.onClick.AddListener(delegate { EvalCode(); });
        txtResult.transform.parent.gameObject.SetActive(false);
    }

    private void EvalCode()
    {
        if (inputCode.text.Equals(code))
        {
            txtResult.text = "Access Granted";
            PlayerPrefs.SetInt("editAccess", 1);
            Invoke("SetShowingFalse", 1);
            Invoke("ClickSlider", 1.1f);
            AM_Controller.instance.userHasMapInputControl = true;
        }
        else
        {
            txtResult.transform.parent.gameObject.SetActive(true);
            txtResult.text = "Invalid code";
        }
    }

    private void SetShowingFalse()
    {
        gameObject.SetActive(false);
    }

    private void ClickSlider()
    {
        AM_Controller.instance.ClickSlider();
    }

    public void Open(string trueCode)
    {
        AM_Controller.instance.userHasMapInputControl = false;
        code = trueCode;
        gameObject.SetActive(true);
    }


}
