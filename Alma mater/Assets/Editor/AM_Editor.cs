﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AM_Editor : MonoBehaviour {
#if UNITY_EDITOR 
    [UnityEditor.MenuItem("AM_Tools/Google Sheets - Open responses")]
    static void OpenSettingsFile() {
        string url = "https://docs.google.com/spreadsheets/d/1zPfT6-DIoYJZ0-YF4BPpLjngw_obvpHUAAyp4j0sNG4/edit#gid=1086245261";
        Application.OpenURL(url);
    }
    [UnityEditor.MenuItem("AM_Tools/Google Sheets - Open input form")]
    static void OpenFormFile() {
        string url = "https://docs.google.com/forms/d/1-VfP6nAa4Z8oN6D1IJg0evqoR0fITHdmT67E_8mQ868/edit#responses";
        Application.OpenURL(url);
    }
    [UnityEditor.MenuItem("AM_Tools/Reset Player Prefs")]
    static void ResetPlayerPrefs() {
        PlayerPrefs.DeleteAll();
    }

    [UnityEditor.MenuItem("AM_Tools/Copy Username")]
    static void CopyUser() {
        GUIUtility.systemCopyBuffer = "almamatermailman@gmail.com";
    }

    [UnityEditor.MenuItem("AM_Tools/Copy Password")]
    static void CopyPass() {
        GUIUtility.systemCopyBuffer = "Die Luft der Freiheit weht";
    }

    [UnityEditor.MenuItem("AM_Tools/ViewPHPWebsite")]
    static void ViewWebsite()
    {
        string url = "https://almamatergame.000webhostapp.com/index.php";
        Application.OpenURL(url);
    }

    [UnityEditor.MenuItem("AM_Tools/EditWebsite")]
    static void EditWebsite()
    {
        string url = "https://files.000webhost.com/";
        Application.OpenURL(url);
    }



#endif
}
