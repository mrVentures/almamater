﻿using UnityEditor;

[CustomEditor(typeof(AM_Button), true)]
public class AM_ButtonEditor : UnityEditor.UI.ButtonEditor
{
    SerializedProperty _onDownProperty;
    SerializedProperty _onUpProperty;

    protected override void OnEnable()
    {
        base.OnEnable();
        _onDownProperty = serializedObject.FindProperty("_onDown");
        _onUpProperty = serializedObject.FindProperty("_onUp");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EditorGUILayout.Space();

        serializedObject.Update();

        EditorGUILayout.PropertyField(_onDownProperty);
        EditorGUILayout.PropertyField(_onUpProperty);

        serializedObject.ApplyModifiedProperties();
    }
}