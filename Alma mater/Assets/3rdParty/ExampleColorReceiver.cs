using UnityEngine;

public class ExampleColorReceiver : MonoBehaviour {
	
    Color color;

	void OnColorChange(HSBColor color) 
	{
        this.color = color.ToColor();
        this.color.a = 1;
        AM_Controller.instance.OnColorSelected( this.color );
	}

    void OnGUI()
    {
		var r = Camera.main.pixelRect;
		var rect = new Rect(r.center.x + r.height / 6 + 50, r.center.y, 100, 100);
    }

	string ToHex(float n)
	{
		return ((int)(n * 255)).ToString("X").PadLeft(2, '0');
	}
}
